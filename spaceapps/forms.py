from django import forms
from .models import Country

#Crear fomularios

class CountryForm(forms.ModelForm):
    class Meta:
        fields = ('id', 'continente', 'region', 'nombre')
        model = Country