from django.db import models

# Create your models here.


class Country(models.Model):
    id = models.CharField(max_length=3, primary_key=True)
    nombre = models.CharField(max_length=52)
    continente = models.CharField(max_length=50)
    region = models.CharField(max_length=26)
    area = models.FloatField()
    independencia = models.SmallIntegerField(null=True)
    poblacion = models.IntegerField()
    expectavida_de_vida = models.FloatField(null=True)
    pib = models.FloatField()
    pib_antiguo = models.FloatField(null=True)
    nombre_local = models.CharField(max_length=45)
    gobierno = models.CharField(max_length=45)
    jefe_de_Estado = models.CharField(max_length=60, null=True)
    capital = models.IntegerField(null=True)
    codigo2 = models.CharField(max_length=2)

    def __str__(self):
        return self.continente


class CountryCity(models.Model):
    id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=35)
    cod_pais = models.CharField(max_length=3)
    distrito = models.CharField(max_length=20)
    poblacion = models.IntegerField()
