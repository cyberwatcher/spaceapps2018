import math
import requests
from django.shortcuts import render
from django.http import HttpResponse
import json
from .models import Country, CountryCity
from django.db import connection

# Create your views here.

def render_index(request):
    countries_continente = Country.objects.values('continente').distinct().order_by('continente')
    countries_nombre = Country.objects.values('nombre').order_by('nombre')
    context = {"countries": countries_continente,"countries_name":countries_nombre}
    return render(request, 'index.html', context)


def filtro_1(request,continent):
    countries_nombre = Country.objects.filter(continente=continent).order_by('nombre')
    resultados = []
    for country in countries_nombre:
        item_json = {}
        # Valor por defecto si no se implementa la opción de renderizado en jQuery
        item_json['nombre'] = country.nombre
        item_json['cod'] = country.id
        resultados.append(item_json)

    datos = json.dumps(resultados)
    mimetype = 'application/json'
    return HttpResponse(datos, mimetype)


def render_index_after_processing(request):
    id_request = execute_single_element_query("SELECT max(R.id) FROM requests R")
    prices_tuple = execute_query("SELECT FD.windmill_count_cost, FD.solarpanel_count_cost FROM final_data FD WHERE FD.id_request = " + str(id_request))
    prices = prices_tuple[0]
    electricity_generated_tuple = execute_query("SELECT FD.windmill_year_power, FD.solarpanel_year_power FROM final_data FD WHERE FD.id_request = " + str(id_request))
    electricity_generated = electricity_generated_tuple[0]
    print(electricity_generated)
    amounts_tuple = execute_query("SELECT FD.windmill_count, FD.solarpanel_count FROM final_data FD WHERE FD.id_request = " + str(id_request))
    amounts = amounts_tuple[0]

    countries_continente = Country.objects.values('continente').distinct().order_by('continente')
    countries_nombre = Country.objects.values('nombre').order_by('nombre')
    context = {"countries": countries_continente, "countries_name": countries_nombre, "prices": prices, "electricity_generated": electricity_generated, "amounts": amounts }
    return context



# It makes calculations out of the available raw data related to the request to get the final data the user is going to see displayed on screen
def process_raw_data(request):
    id_request = execute_single_element_query("SELECT max(R.id) FROM requests R")
    required_gwh = execute_single_element_query("SELECT R.required_gwh FROM requests R WHERE id = " + str(id_request))

    # Eolic calculations begin
    windmill_price = execute_single_element_query("SELECT R.windmill_price FROM requests R WHERE R.id = " + str(id_request))
    rotor_diameter = execute_single_element_query("SELECT R.rotor_diameter FROM requests R WHERE R.id = " + str(id_request))
    average_windspeed = execute_single_element_query("SELECT avg(RW.average_speed) FROM raw_wind RW WHERE RW.id_request = " + str(id_request))
    air_density = 1.225
    surface = math.pow(rotor_diameter / 2, 2) * math.pi
    hours_a_year = 8760
    betz_limit = 0.40

    power_density = ((0.5 * air_density) * math.pow(average_windspeed, 3)) / 1000
    windmill_year_power = (((power_density * hours_a_year) * surface) * betz_limit) / 1000000

    windmill_amount = math.floor(required_gwh / windmill_year_power)
    windmill_amount_cost = windmill_amount * windmill_price
    #Eolic operations end

    # Solar operations begin
    peak_hours = execute_single_element_query("SELECT RS.peak_hours FROM raw_solar RS WHERE RS.id_request = " + str(id_request))
    solarpanel_power = execute_single_element_query("SELECT R.solarpanel_power FROM requests R WHERE R.id = " + str(id_request))
    solarpanel_price = execute_single_element_query("SELECT R.solarpanel_price FROM requests R WHERE R.id = " + str(id_request))

    solarpanel_day_power = solarpanel_power * peak_hours
    solarpanel_year_power = (solarpanel_day_power * 365) / 1000000

    solarpanel_amount = math.floor(required_gwh / solarpanel_year_power)
    solarpanel_amount_cost = solarpanel_amount * solarpanel_price
    # Solar operations end

    #Insert data into database
    execute_query_no_return("INSERT INTO final_data (windmill_count, solarpanel_count, required_gwh, windmill_count_cost, solarpanel_count_cost, windmill_year_power, solarpanel_year_power, id_request) VALUES (" + str(windmill_amount) +", " + str(solarpanel_amount) + ", " + str(required_gwh) + ", " + str(windmill_amount_cost) + ", " + str(solarpanel_amount_cost) + "," + str(windmill_year_power) + ", " + str(solarpanel_year_power) + ", " + str(id_request) + ")")
    context = render_index_after_processing(request)
    return context

# It executes a sql query that returns a single element instead of a tuple
def execute_single_element_query(sql):
    average_regs = execute_query(sql)
    average_reg = average_regs[0]
    average = average_reg[0]

    return average


# It executes a sql query and doesn't return a value, potentially useful with inserts and updates
def execute_query_no_return(sql):
    cursor = connection.cursor()
    sql_sentence = sql
    cursor.execute(sql_sentence)


# It executes a sql query and returns its result as a tuple
def execute_query(sql):
    cursor = connection.cursor()
    sql_sentence = sql
    cursor.execute(sql_sentence)
    result = cursor.fetchall()

    return result

def filtro_2(request,pais):
    ciudades_nombre = CountryCity.objects.filter(cod_pais=pais).order_by('nombre')
    resultados = []
    for ciudad in ciudades_nombre:
        item_json = {}
        # Valor por defecto si no se implementa la opción de renderizado en jQuery
        item_json['nombre'] = ciudad.nombre
        resultados.append(item_json)

    datos = json.dumps(resultados)
    mimetype = 'application/json'
    return HttpResponse(datos, mimetype)

def paso_1(request):
    ciudad = request.POST['in_ciudad']
    consumo = request.POST['in_consumo']
    preciopl = request.POST['in_preciopl']
    preciomo = request.POST['in_preciomo']
    diametro_ro = request.POST['in_diametro_ro']
    potencia_placa = request.POST['in_potencia_placa']
    horas_sol = request.POST['in_horas_sol']
    cursor = connection.cursor()
    sql_sentence = "INSERT INTO requests (required_gwh, windmill_price, solarpanel_price, rotor_diameter, solarpanel_power, city) VALUES (" + str(consumo) + ", " + str(preciomo) + ", " + str(preciopl) + ", " + str(diametro_ro) + "," + str(potencia_placa) + ", '" + str(ciudad) + "')"
    cursor.execute(sql_sentence)
    context = insert_data(request, ciudad, horas_sol)
    print(context)
    return render(request, 'index.html', context)


def insert_data(request, ciudad, horas_sol):
    url = "http://api.openweathermap.org/data/2.5/weather?q="+ciudad+"&APPID=f85a9829e310610b52daa6e862728488"
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36 OPR/56.0.3051.31'}
    texto = ""
    with requests.Session() as s:
        s.headers = headers
        texto = s.get(url).text

    parsed_json = json.loads(texto)
    avegare_speed = parsed_json['wind']['speed']
    longitud = parsed_json['coord']['lon']
    latitud = parsed_json['coord']['lat']
    air_density = 1.225
    cursor = connection.cursor()
    id_request = execute_single_element_query("SELECT max(R.id) FROM requests R")
    sql_sentence = "INSERT INTO raw_wind (latitude, longitude, average_speed, air_density, id_request) VALUES (" + str(latitud) + ", " + str(longitud) + ", " + str(avegare_speed) + ", " + str(air_density)+", " + str(id_request)+")"
    cursor.execute(sql_sentence)

    sql_sentence = "INSERT INTO raw_solar (latitude, longitude, peak_hours, id_request) VALUES (" + str(latitud) + ", " + str(longitud) + ", " + str(horas_sol) + ", " + str(id_request) + ")"
    cursor.execute(sql_sentence)
    context = process_raw_data(request)
    return context

