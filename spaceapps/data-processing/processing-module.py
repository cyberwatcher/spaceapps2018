from django.db import connection
import math


# It makes calculations out of the available raw data related to the request to get the final data the user is going to see displayed on screen
def process_raw_data():
    id_request = execute_single_element_query("SELECT max(R.id) FROM requests R")
    required_gwh = execute_single_element_query("SELECT R.required_gwh FROM requests R WHERE id = " + str(id_request))

    # Eolic calculations begin
    windmill_price = execute_single_element_query("SELECT R.windmill_price FROM requests R WHERE R.id = " + str(id_request))
    rotor_diameter = execute_single_element_query("SELECT R.rotor_diameter FROM requests R WHERE R.id = " + str(id_request))
    average_windspeed = execute_single_element_query("SELECT avg(RW.average_speed) FROM raw_wind RW WHERE RW.id_request = " + str(id_request))
    air_density = 1.225
    surface = math.pow(rotor_diameter / 2, 2) * math.pi
    hours_a_year = 8760
    betz_limit = 0.40

    power_density = ((0.5 * air_density) * math.pow(average_windspeed, 3)) / 1000
    windmill_year_power = (((power_density * hours_a_year) * surface) * betz_limit) / 1000000

    windmill_amount = math.floor(required_gwh / windmill_year_power)
    windmill_amount_cost = windmill_amount * windmill_price
    #Eolic operations end

    # Solar operations begin
    solarpanel_price = execute_query("SELECT R.solarpanel_price FROM requests R WHERE R.id = " + str(id_request))
    peak_hours = execute_single_element_query("SELECT RS.peak_hours FROM raw_solar RS WHERE RS.id = " + str(id_request))
    solarpanel_power = execute_single_element_query("SELECT R.solarpanel_power FROM requests R WHERE R.id = " + str(id_request))
    solarpanel_price = execute_single_element_query("SELECT R.solarpanel_price FROM requests R WHERE R.id = " + str(id_request))

    solarpanel_day_power = solarpanel_power * peak_hours
    solarpanel_year_power = (solarpanel_day_power * 365) / 1000000

    solarpanel_amount = math.floor(required_gwh / solarpanel_year_power)
    solarpanel_amount_cost = solarpanel_amount * solarpanel_price
    # Solar operations end

    #Insert data into database
    execute_query_no_return("INSERT INTO final_data (windmill_count, solarpanel_count, required_gwh, windmill_count_cost, solarpanel_count_cost, windmill_year_power, solarpanel_year_power, id_request) VALUES (" + str(windmill_amount) +", " + str(solarpanel_amount) + ", " + str(required_gwh) + ", " + str(windmill_amount_cost) + ", " + str(solarpanel_amount_cost) + "," + str(windmill_year_power) + ", " + str(solarpanel_year_power) + ", " + str(id_request) + ")")


# It executes a sql query that returns a single element instead of a tuple
def execute_single_element_query(sql):
    average_regs = execute_query(sql)
    average_reg = average_regs[0]
    average = average_reg[0]

    return average


# It executes a sql query and doesn't return a value, potentially useful with inserts and updates
def execute_query_no_return(sql):
    cursor = connection.cursor()
    sql_sentence = sql
    cursor.execute(sql_sentence)


# It executes a sql query and returns its result as a tuple
def execute_query(sql):
    cursor = connection.cursor()
    sql_sentence = sql
    cursor.execute(sql_sentence)
    result = cursor.fetchall()

    return result