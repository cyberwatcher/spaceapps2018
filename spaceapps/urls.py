"""SpaceApps2018 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from . import views

#Crear urls

urlpatterns = [
    url(r'^$', views.render_index, name="index"),
    url(r'^filtro_1/(?P<continent>[^/]+)/$', views.filtro_1, name="filtro_1"),
    url(r'^filtro_2/(?P<pais>[^/]+)/$', views.filtro_2, name="filtro_2"),
    url(r'^paso_1/$', views.paso_1, name="paso_1"),
]
